import java.net.URI
import java.time.Duration

plugins {
    kotlin("jvm") version "1.4.32"
    id("org.jetbrains.dokka") version "1.4.30"
    id("com.github.ben-manes.versions") version "0.38.0"
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "1.0.0"
    id("net.researchgate.release") version "2.8.1"
    id("org.jmailen.kotlinter") version "3.4.0"
}

repositories {
    mavenCentral()
    // kotlinx-html, used by dokka, is not on maven central yet
    @Suppress("deprecation")
    jcenter()
}

group = "org.mpierce.ktor"

val deps by extra {
    mapOf(
        "junit" to "5.7.1",
        "ktor" to "1.5.2"
    )
}

dependencies {
    implementation(kotlin("stdlib"))

    api("io.ktor:ktor-server-core:${deps["ktor"]}")

    testImplementation("org.mpierce.http.client.threadlocalheader:async-http-client:0.2.0")
    testImplementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    testRuntimeOnly("org.slf4j:slf4j-simple:1.7.30")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8

    withSourcesJar()
}

tasks {
    test {
        useJUnitPlatform()

    }

    register<Jar>("docJar") {
        from(project.tasks["dokkaHtml"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(provider { project.tasks.named("publishToSonatype") })
        dependsOn(provider { project.tasks.named("closeSonatypeStagingRepository") })
    }
}

publishing {
    publications {
        register<MavenPublication>("sonatype") {
            from(components["java"])
            artifact(tasks["docJar"])
            // sonatype required pom elements
            pom {
                name.set("${project.group}:${project.name}")
                description.set(name)
                url.set("https://bitbucket.org/marshallpierce/kotlin-new-relic")
                licenses {
                    license {
                        name.set("Copyfree Open Innovation License 0.5")
                        url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                    }
                }
                developers {
                    developer {
                        id.set("marshallpierce")
                        name.set("Marshall Pierce")
                        email.set("575695+marshallpierce@users.noreply.github.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://bitbucket.org/marshallpierce/kotlin-new-relic")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/kotlin-new-relic.git")
                    url.set("https://bitbucket.org/marshallpierce/kotlin-new-relic")
                }
            }
        }
    }

    // A safe throw-away place to publish to:
    // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
    repositories {
        maven {
            name = "localDebug"
            url = URI.create("file:///${project.buildDir}/repos/localDebug")
        }
    }
}


// don't barf for devs without signing set up
if (project.hasProperty("signing.keyId")) {
    signing {
        sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}
