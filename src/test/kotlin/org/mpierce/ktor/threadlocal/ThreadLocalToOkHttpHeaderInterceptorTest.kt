package org.mpierce.ktor.threadlocal

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallId
import io.ktor.features.callId
import io.ktor.request.header
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.future.await
import kotlinx.coroutines.withContext
import org.asynchttpclient.Dsl
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mpierce.httpclient.threadlocal.asynchttpclient.AsyncHttpClientAdapter
import org.mpierce.httpclient.threadlocal.core.SingleHeaderAdder
import java.util.concurrent.Executors

internal class ThreadLocalToOkHttpHeaderInterceptorTest {

    private val proxyPort = 12000
    private val destinationPort = 12001

    // maybe you want to add a call id
    private val callIdHeaderName = "x-call-id"

    // and also some trace data?
    private val traceHeaderName = "x-trace"

    private val executor = Executors.newCachedThreadPool()

    @AfterEach
    internal fun tearDown() {
        executor.shutdown()
    }

    @Test
    internal fun featureSetsThreadLocalForInterceptor() {
        // use thread local interceptor to forward headers to destination
        val callId = ThreadLocal.withInitial<String?> { null }
        val trace = ThreadLocal.withInitial<String?> { null }

        val proxyClient = Dsl.config()
            .addRequestFilter(AsyncHttpClientAdapter(SingleHeaderAdder(callIdHeaderName, callId, { it })))
            .addRequestFilter(
                AsyncHttpClientAdapter(SingleHeaderAdder(traceHeaderName, trace, { it }))
            )
            .build()
            .let { Dsl.asyncHttpClient(it) }

        // set up a proxy server that should pass along the call id
        val proxy = embeddedServer(Netty, port = proxyPort) {
            install(CallId) {
                retrieveFromHeader(callIdHeaderName)
                generate { "generated-proxy" }
            }
            install(ThreadLocalCoroutineContext) {
                // integrate with call id
                threadLocal(callId) { it.callId }
                // but also with just a plain header
                threadLocal(trace) { it.request.header(traceHeaderName) }
            }
            install(Routing) {
                get("/proxy") {
                    // on a different thread
                    val body = withContext(executor.asCoroutineDispatcher()) {
                        proxyClient.prepareGet("http://localhost:$destinationPort/data")
                            .execute()
                            .toCompletableFuture()
                            .await()
                            .responseBody
                    }

                    call.respond(body)
                }
            }
        }

        proxy.start(wait = false)
        try {
            // and a destination server that the proxy will call
            val destination = embeddedServer(Netty, port = destinationPort) {
                install(CallId) {
                    retrieveFromHeader(callIdHeaderName)
                    generate { "generated-destination" }
                }
                install(Routing) {
                    get("/data") {
                        // returns call id and trace so we can tell if it made it
                        call.respond("call id: ${call.callId}, trace: ${call.request.header(traceHeaderName)}")
                    }
                }
            }

            destination.start(wait = false)
            try {
                val externalClient = Dsl.asyncHttpClient()

                val resp = externalClient.prepareGet("http://localhost:$proxyPort/proxy")
                    // these headers should get picked up and set in thread locals and then set via interceptors
                    .addHeader(callIdHeaderName, "random-call-id")
                    .addHeader(traceHeaderName, "span1")
                    .execute()
                    .get()

                assertEquals("call id: random-call-id, trace: span1", resp.responseBody)
            } finally {
                destination.stop(10, 10)
            }
        } finally {
            proxy.stop(10, 10)
        }
    }
}
