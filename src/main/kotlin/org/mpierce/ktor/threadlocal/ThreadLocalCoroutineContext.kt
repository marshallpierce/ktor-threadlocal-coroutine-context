package org.mpierce.ktor.threadlocal

import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.application.call
import io.ktor.util.AttributeKey
import kotlinx.coroutines.ThreadContextElement
import kotlinx.coroutines.asContextElement
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * A feature to set one or more thread locals in the coroutine context of each request.
 *
 * In the configuration block for the feature, call `threadLocal()` for each thread local you want to be set.
 */
class ThreadLocalCoroutineContext private constructor(private val threadLocals: List<ThreadLocalConfig<*>>) {
    class Configuration {
        internal val threadLocals = mutableListOf<ThreadLocalConfig<*>>()

        /**
         * @param threadLocal the threadlocal to set
         * @param dataProvider block called on the [ApplicationCall]. The value returned is set as the value of the
         * threadlocal for the request coroutine context.
         */
        fun <T> threadLocal(threadLocal: ThreadLocal<T>, dataProvider: suspend (ApplicationCall) -> T) {
            this.threadLocals.add(ThreadLocalConfig(threadLocal, dataProvider))
        }
    }

    companion object Feature : ApplicationFeature<Application, Configuration, ThreadLocalCoroutineContext> {
        override val key: AttributeKey<ThreadLocalCoroutineContext> = AttributeKey("ThreadLocal coroutine context")

        override fun install(
            pipeline: Application,
            configure: Configuration.() -> Unit
        ): ThreadLocalCoroutineContext {
            val config = Configuration().apply(configure)
            val interceptors = config.threadLocals.toList()
            if (interceptors.isEmpty()) {
                throw IllegalStateException("Must configure at least one thread local")
            }

            val feature = ThreadLocalCoroutineContext(interceptors)

            pipeline.intercept(ApplicationCallPipeline.Features) {
                // For each interceptor, the appropriate thread local will be set when any coroutine with this context
                // is running
                val context = feature.threadLocals
                    // avoid creating intermediate collection since this is hot code
                    .fold(EmptyCoroutineContext as CoroutineContext) { context, foo ->
                        context + foo.asContext(call)
                    }

                // withContext allocates a lambda internally but presumably you're using this feature because you
                // actually have data you want to propagate, so doing a check for empty context to avoid withContext
                // seems like overkill
                withContext(context) {
                    proceed()
                }
            }

            return feature
        }
    }
}

internal class ThreadLocalConfig<T>(
    private val threadLocal: ThreadLocal<T>,
    private val block: suspend (ApplicationCall) -> T
) {
    /**
     * Wraps the hand-off from the block to the thread local, allowing it to be called safely on a polymorphic
     * collection even though `T` for each instance is not known at that point.
     */
    suspend fun asContext(call: ApplicationCall): ThreadContextElement<T> {
        return threadLocal.asContextElement(block(call))
    }
}
