[ ![Download](https://api.bintray.com/packages/marshallpierce/maven/ktor-threadlocal-coroutine-context/images/download.svg) ](https://bintray.com/marshallpierce/maven/ktor-threadlocal-coroutine-context/_latestVersion) 

## What is it?

A [Ktor](https://ktor.io/) feature to coordinate setting `ThreadLocal`s in the coroutine context for each request. 

## Why is that useful?

Coroutines are great, but sometimes you need to integrate with libraries that expect to have contextual information provided via a `ThreadLocal`. This library makes it easy to specify `ThreadLocal`s that should be set in a particular way for each request's coroutine context. The `ThreadLocal` will be set correctly for the coroutine context used for each request. In other words, whatever thread is used for the coroutine for a request, and all child coroutines (via `async`, `launch`, etc), those threads will have the right value in the `ThreadLocal`. You can also access the the data via `couroutineContext` rather than by accessing the thread local.

## Usage

Suppose you have the following scenario: incoming requests to your Ktor service have an `x-call-id` header, and you'd like to propagate that header to other services when you make requests to them so that you can match up logging across all services for the same call id.

To parse the incoming request header, use the built-in [`CallId`](https://ktor.io/servers/features/call-id.html) feature:

```kotlin
// in ktor app config
install(CallId) {
    // ... see call id docs for config
    retrieveFromHeader(callIdHeaderName)
}
```

Now, you need to add the same header to outbound requests. That's where this library can help.

If your http client is built around Kotlin coroutines, then you could access the call id via coroutine context using [ktor-call-id-coroutine-context](https://bitbucket.org/marshallpierce/ktor-call-id-coroutine-context/src/master/), but most http clients in the wild are plain old Java, so all they have is `ThreadLocal`. 

# Example

If you're using a Java (not coroutine aware) http client like [Async Http Client](https://github.com/AsyncHttpClient/async-http-client) or 
[OkHttp](http://square.github.io/okhttp/), you can use [http-client-threadlocal-header](https://bitbucket.org/marshallpierce/http-client-threadlocal-header/src/master/) to set outbound headers via a `ThreadLocal`, so put all together we have:

```kotlin
// somewhere in your service startup, make a threadlocal
val callIdThreadLocal = ThreadLocal.withInitial<String?> { null }

// Set up a HeaderModifier to add a single header based on the ThreadLocal
// In this case, we have a ThreadLocal<String> so we don't need to transform it
// to get a String to put into a header, so we use the identity function.
val headerAdder = SingleHeaderAdder("x-request-id", currentRequestId, 
        Function.identity())

// Use that with as many http clients as you like. 
// Here, we use Async Http Client with the corresponding adapter:
val ahc = Dsl.asyncHttpClient(Dsl.config()
        .addRequestFilter(new AsyncHttpClientAdapter(headerAdder))
        .build())

// Or OkHttp with a different adapter
var okhttp = OkHttpClient.Builder()
         .addNetworkInterceptor(new OkHttpAdapter(headerAdder))
         .build();

// in ktor app setup:
install(ThreadLocalCoroutineContext) {    
    threadLocal(callIdThreadLocal) { call -> 
        // for each request, set the thread local (for that coroutine context) to be the call id
        call.callId 
    }
    // can set up more threadlocals too, not just one
}
```

Now, in some endpoint logic, the header will get automatically applied:

```kotlin
get("/foo") {
    // header gets added for you
    httpClient.newCall(Request.Builder() .url("http://the-url") .build())
            .execute()

    coroutineScope {
        launch {
            // will also work in child coroutines
            httpClient.newCall(Request.Builder() .url("http://the-url") .build())
                        .execute()        
        }    
    }

    withContext(Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
        // even if you force it to be on a different thread, it'll still work    
        httpClient.newCall(Request.Builder().url("http://the-url") .build())
                    .execute()        
    }
}
```
